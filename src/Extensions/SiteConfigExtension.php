<?php

namespace ChitoSystems\App\Extensions;

use SilverStripe\Forms\EmailField;
use SilverStripe\Forms\FieldList;
use SilverStripe\ORM\DataExtension;

class SiteConfigExtension extends DataExtension {

    private static $db = [
        'FromEmail' => 'Varchar(255)',
        'ReplyToEmail' => 'Varchar(255)',
    ];

    private static $has_one = [];

    private static $owns = [];

    /**
     * @param  \SilverStripe\Forms\FieldList  $fields
     */
    public function updateCMSFields ( FieldList $fields )
    {
        $fields->addFieldsToTab( 'Root.EmailAddresses', [
            EmailField::create( 'FromEmail' ),
            EmailField::create( 'ReplyToEmail' ),
        ] );


    }

    public function getFromEmailAddress(){
        return $this->owner->FromEmail;
    }

    public function getReplyToEmailAddress(){
        return $this->owner->ReplyToEmail;
    }

}
