<?php

namespace ChitoSystems\App\Extensions;

use SilverStripe\ORM\DataExtension;

class FormActionExtension extends DataExtension
{
    public function onBeforeRender($field)
    {
        $field->addExtraClass('btn btn-primary');

    }
}
