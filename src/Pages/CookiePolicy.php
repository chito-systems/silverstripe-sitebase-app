<?php

namespace ChitoSystems\App\Pages;


use SilverStripe\ORM\DataObject;

class CookiePolicy extends LegalPage
{
    private static $table_name = 'CookiePolicy';

    /**
     * @param false $action
     *
     * @return mixed
     */
    public static function findLink($action = false)
    {
        if ( !$page = DataObject::get_one(__CLASS__) ) {
            user_error('No CookiePolicy Page was found. Please create one in the CMS!', E_USER_ERROR);

        }

        return $page->Link($action);
    }


    public function canCreate($member = null, $context = [])
    {
        return !DataObject::get_one(__CLASS__);
    }
}
