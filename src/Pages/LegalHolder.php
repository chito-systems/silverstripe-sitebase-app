<?php

namespace ChitoSystems\App\Pages;

use Page;
use SilverStripe\ORM\DataObject;

class LegalHolder extends Page
{

    private static $table_name = 'LegalHolder';


    public function canCreate($member = null, $context = [])
    {
        return !DataObject::get_one(__CLASS__);
    }
}

