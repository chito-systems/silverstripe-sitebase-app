<?php

namespace ChitoSystems\App\Pages;

use ChitoSystems\App\Models\VideoResource;
use ChitoSystems\Silverstripe\AppBase\Core\SiteManager;
use Page;
use SilverStripe\Forms\GridField\GridField;
use SilverStripe\ORM\DataObject;

/**
 * @method VideoResources()
 */
class HomePage extends Page {

    private static $table_name = 'HomePage';

    /**
     * @var string[]
     */
    private static $has_many = [
        'VideoResources' => VideoResource::class,
    ];

    public static function findLink ( $action = false )
    {
        if ( !$page = DataObject::get_one( __CLASS__ ) ) {
            return '#';
        }

        return $page->Link( $action );
    }

    public function canCreate ( $member = null, $context = [] )
    {
        return !DataObject::get_one( __CLASS__ );
    }

    public function getCMSFields ()
    {
        $f = parent::getCMSFields();

        $f->addFieldToTab( 'Root.VideoResources', GridField::create( 'VideoResources', 'VideoResources', $this->VideoResources(), SiteManager::getGridFieldConfig( 'Sort' ) ) );

        return $f;
    }

    public function Video ()
    {
        return $this->VideoResources()->sort( 'Rand()' )->first();
    }
}
