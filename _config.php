<?php

use SilverStripe\Control\Director;
use SilverStripe\CMS\Model\SiteTree;
use SilverStripe\Core\Config\Config;
use SilverStripe\Control\Session;
use SilverStripe\Core\Environment;
use SilverStripe\View\SSViewer;
use SilverStripe\ORM\Search\FulltextSearchable;
use SilverStripe\Control\Email\Email;
use SilverStripe\Assets\File;

global $project;
$project = basename(__DIR__);
require_once( 'conf/ConfigureFromEnv.php' );

date_default_timezone_set('Africa/Harare');
define('PROJECT', project());

$baseURL = Director::protocolAndHost();
$baseURL = preg_replace('#^(http(s)?://)?w{3}\.#', '$1', $baseURL);
$parse_url = parse_url($baseURL);
$raw_host = explode('.', $parse_url[ 'host' ]);
$domain = $parse_url[ 'host' ] ?? '';
$subdomain = $raw_host[ 0 ];

if ( preg_match('/(?P<domain>[a-z0-9][a-z0-9\-]{1,63}\.[a-z\.]{2,6})$/i', $domain, $regs) ) {
    $true_domain = $regs[ 'domain' ];
    Config::modify()->set(Session::class, 'cookie_domain', $true_domain);
}

if ( Director::isLive() ) {
    Director::forceSSL();
    Director::forceWWW();
}

if ( Director::isDev() ) {
    //SSViewer::flush_template_cache();
    error_reporting(E_ERROR | E_WARNING | E_PARSE & ~( E_STRICT | E_NOTICE | E_DEPRECATED ));
}

FulltextSearchable::enable();
SSViewer::setRewriteHashLinksDefault(false);
